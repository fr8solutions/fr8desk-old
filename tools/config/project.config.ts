import { join } from 'path';

import { SeedConfig } from './seed.config';
import { ExtendPackages } from './seed.config.interfaces';

/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {

  PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');

  constructor() {
    super();
    // this.APP_TITLE = 'Put name of your app here';

    /* Enable typeless compiler runs (faster) between typed compiler runs. */
    // this.TYPED_COMPILE_INTERVAL = 5;

    // Add `NPM` third-party libraries to be injected/bundled.
    this.NPM_DEPENDENCIES = [
      ...this.NPM_DEPENDENCIES,
      //{src: 'jquery/dist/jquery.min.js', inject: 'libs'},
      //{src: 'lodash/lodash.min.js', inject: 'libs'},
     
      { src: 'bootstrap/dist/js/bootstrap.min.js', inject: 'libs' },
      { src: 'bootstrap/dist/css/bootstrap.min.css', inject: true }, // inject into css section
      { src: 'bootstrap/dist/css/bootstrap-theme.min.css', inject: true }, // inject into css section
      { src: 'bootstrap/dist/css/bootstrap-theme.min.css.map', inject: true }, // inject into css section
    
    ];

    // Add `local` third-party libraries to be injected/bundled.
    this.APP_ASSETS = [
      ...this.APP_ASSETS,
      // {src: `${this.APP_SRC}/your-path-to-lib/libs/jquery-ui.js`, inject: true, vendor: false}
      // {src: `${this.CSS_SRC}/path-to-lib/test-lib.css`, inject: true, vendor: false},
    ];

    let additionalPackages: ExtendPackages[] = [

      
      {
        name: 'ng2-bootstrap',
        path: 'node_modules/ng2-bootstrap/bundles/ng2-bootstrap.umd.min.js'
      },

      // required for prod build
      {
        name: 'ng2-bootstrap/*',
        path: 'node_modules/ng2-bootstrap/bundles/ng2-bootstrap.umd.min.js'
      },

      {
        name: 'lodash',
        path: 'node_modules/lodash/lodash.min.js',
        
      },
      {
        name: 'rxjs',
        path: 'node_modules/rxjs',
         packageMeta: {
          main: 'Rx.js',
          defaultExtension: 'js'
        }
        
      },

      // required for prod build
      {
        name: 'ng2-restangular',
        path: 'node_modules/ng2-restangular/dist/esm/src',
        packageMeta: {
          main: 'index.js',
          defaultExtension: 'js'
        }
       
        
      },

      // required for place autocomplete
      {
        name: 'ng2-google-place-autocomplete',
        path: 'node_modules/ng2-google-place-autocomplete/src',
        packageMeta: {
          main: 'index.ts',
          defaultExtension: 'ts'
        }
       
        
      },

      // required for date time picker
      {
        name: 'ng2-datetime-picker',
        path: 'node_modules/ng2-datetime-picker/dist',
        packageMeta: {
          main: 'ng2-datetime-picker.umd.js',
          defaultExtension: 'js'
        }
       
        
      },
      // for ng2 notifications

      {
        name: 'angular2-notifications',
        path: 'node_modules/angular2-notifications',
        packageMeta: {
          main: 'components.js',
          defaultExtension: 'js'
        }
      },

      // mandatory dependency for ng2-bootstrap datepicker 
      {
        name: 'moment',
        path: 'node_modules/moment',
        packageMeta: {
          main: 'moment.js',
          defaultExtension: 'js'
        }
      }
    ];
    this.addPackagesBundles(additionalPackages);
  }

}
