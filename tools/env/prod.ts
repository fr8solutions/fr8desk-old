import { EnvConfig } from './env-config.interface';

const ProdConfig: EnvConfig = {
  ENV: 'PROD',
  API: 'http://dapi.fr8.in/api/v2',
  APP_KEY: 'cfd75367c98c1541ae856ddce657a256ab7a4a289727e36849d5ff2e22c7e95c'
};

export = ProdConfig;

