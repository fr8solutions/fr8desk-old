import { EnvConfig } from './env-config.interface';

const BaseConfig: EnvConfig = {
  // Sample API url
  APP_KEY: 'e1c1734b3b0615595b18ba0b11d848ce250f77552cece647032b25f3874e3f08'
};

export = BaseConfig;

