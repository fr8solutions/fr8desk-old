import { EnvConfig } from './env-config.interface';

const DevConfig: EnvConfig = {
  ENV: 'DEV',
  API: 'http://dapi.fr8.in/api/v2/',
  APP_KEY: 'e1c1734b3b0615595b18ba0b11d848ce250f77552cece647032b25f3874e3f08'
};

export = DevConfig;

