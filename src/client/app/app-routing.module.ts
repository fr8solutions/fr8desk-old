import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoadComponent } from './pages/load/load.component';
import { MainHomeComponent } from './main_home/main_home.component';
import { Register } from './register/register.component';
import { Login } from './login/login.component';
import { BuildLoadComponent } from './pages/load/buildload/buildload.component';
import { ViewLoadComponent } from './pages/load/viewload/viewload.component';
import { PagesComponent } from './pages/pages.component';

//for authentication
import { AuthGuard } from './shared/auth/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forRoot([
      /* define app module routes here, e.g., to lazily load a module
         (do not place feature module routes here, use an own -routing.module.ts in the feature instead)
       */
      { path: 'pages', component: PagesComponent ,canActivate:[AuthGuard],
        children: [
          { path: 'load', component: ViewLoadComponent,canActivate:[AuthGuard] },
      { path: 'buildload', component: BuildLoadComponent,canActivate:[AuthGuard] },
    ]
   },
      { path: '', component: MainHomeComponent },
      { path: 'register', component: Register },
      { path: 'login', component: Login }
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

