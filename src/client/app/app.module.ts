import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HomeModule } from './home/home.module';
import { MainHomeModule } from './main_home/main_home.module';
import { RegisterModule } from './register/register.module';
import { LoginModule } from './login/login.module';


import { SharedModule } from './shared/shared.module';
import { SidebarModule } from './shared/sidebar/sidebar.module';

import { Config } from './shared/config/env.config';

//for angular restangular
import { RestangularModule } from 'ng2-restangular';

//for authentication
import { AuthGuard } from './shared/auth/auth-guard.service';
import { ReactiveFormsModule }   from '@angular/forms';

import {SimpleNotificationsModule} from 'angular2-notifications';

import { PagesModule } from './pages/pages.module'








// Function for settting the default restangular configuration
export function RestangularConfigFactory(RestangularProvider:any) {
  RestangularProvider.setBaseUrl(Config.API);
  RestangularProvider.setDefaultHeaders(
   { 'X-DreamFactory-Api-Key': Config.APP_KEY }
  );

  //because our data has resource
  RestangularProvider.addResponseInterceptor((data: any, operation:any, what:any, url:any, response: any ) => {
    return data.resource ? data.resource : data;
  });

}

/*Appolo client for GraphQL
import { ApolloClient, createNetworkInterface } from 'apollo-client';
import { ApolloModule } from 'apollo-angular';

// by default, this client will send queries to `/graphql` (relative to the URL of your app)
const client = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: 'http://api.fr8.in/api/v2',
    opts: {
    // Additional fetch options like `credentials` or `headers`
    headers: {'X-DreamFactory-Api-Key': "cfd75367c98c1541ae856ddce657a256ab7a4a289727e36849d5ff2e22c7e95c"}
  }
  }),
});

export function provideClient(): ApolloClient {
  return client;
}*/
@NgModule({
  imports: [ BrowserModule, HttpModule, LoginModule, ReactiveFormsModule, AppRoutingModule, PagesModule, RegisterModule, HomeModule, MainHomeModule, SidebarModule
  , RestangularModule.forRoot(RestangularConfigFactory), SharedModule.forRoot(),SimpleNotificationsModule.forRoot()],
  declarations: [AppComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  },AuthGuard],
  bootstrap: [AppComponent]

})
export class AppModule { }
