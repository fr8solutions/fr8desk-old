import { Component } from '@angular/core';
import { AuthGuard } from '../auth/auth-guard.service';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-navbar',
  templateUrl: 'table.component.html',
  styleUrls: ['table.component.css'],
})
export class TableComponent { 


  constructor (private auth: AuthGuard){}
   
  
}