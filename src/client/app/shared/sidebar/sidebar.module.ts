import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Sidebar } from './sidebar.component';
import { SidebarContainer } from './sidebar-container.component';
import { SidebarService } from './sidebar.service';
import { CloseSidebar } from './close.directive';
import { DemoComponent } from './demo';

@NgModule({
  declarations: [Sidebar, SidebarContainer, CloseSidebar,DemoComponent],
  imports: [CommonModule],
  exports: [Sidebar, SidebarContainer, CloseSidebar,DemoComponent],
  providers: [SidebarService]
})
export class SidebarModule {}
