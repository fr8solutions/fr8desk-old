import { Component } from '@angular/core';
import { AuthGuard } from '../auth/auth-guard.service';
//import { Modal } from 'angular2-modal/plugins/bootstrap';
import { AuthenticationService } from '../auth/auth-service';

/**
 * This class represents the toolbar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-toolbar',
  templateUrl: 'toolbar.component.html',
  styleUrls: ['toolbar.component.css']
})
export class ToolbarComponent { 
   constructor (private auth: AuthGuard, private authService: AuthenticationService ){}

logout(){
  this.authService.logout();
}

}