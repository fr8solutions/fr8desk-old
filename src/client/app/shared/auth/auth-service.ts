import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Restangular } from 'ng2-restangular';
import { ActivatedRoute, Router } from '@angular/router';


@Injectable()
export class AuthenticationService {
    public currentUser: Array<any>[];
    public data1: Array<any>[];
    
    constructor(private activatedRoute: ActivatedRoute,public restangular: Restangular, private http: Http, private router: Router) {
        // set token if saved in local storage
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //this.token = currentUser && currentUser.token;
    }

    login(form:any): Observable<boolean> {
        // return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
        let obs= this.restangular.all('user/session').post(form);

            obs.subscribe((res: any) => {
                //this.data1 = res.json();
                //storing as JSON String
                localStorage.setItem('currentUser', JSON.stringify(res));
                localStorage.setItem('id_token', res.session_token);
                return true;
            },
            (err: any) => {
                //Here you can catch the error
                return false
            },
            () => {},
            );
            return obs;

    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.currentUser = null;
        localStorage.removeItem('currentUser');
         localStorage.removeItem('id_token');
         this.router.navigate(["/"],{ relativeTo: this.activatedRoute });
    }
    
    register(rform:any): Observable<boolean> {
        // return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
        //var session_token: any;
        let obs= this.restangular.all('user/register').post(rform);

            obs.subscribe((res: Response) => {
                //storing as JSON String
               // localStorage.setItem('currentUser', JSON.stringify(rform));
                //return true;
                alert("true");
            },
            (err: any) => {
                //Here you can catch the error
                return false
            },
            () => {},
            );
            return obs;
    }

    verify(verform:any): Observable<boolean> {
        // return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
        //var session_token: any;
        let obs= this.restangular.all('user/password?login=true').post(verform);

            obs.subscribe((res: any) => {
                //storing as JSON String
                let session_token: any;
                localStorage.setItem('currentUser', JSON.stringify(res));
                localStorage.setItem('id_token', JSON.stringify(res.session_token));
                return true;
            },
            (err: any) => {
                //Here you can catch the error
                return false
            },
            () => {},
            );
            return obs;

    }
}