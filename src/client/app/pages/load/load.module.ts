import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadComponent } from './load.component';
import { LoadRoutingModule } from './load-routing.module';
import { BuildLoadRoutingModule } from './buildload/buildload-routing.module';

import { ViewLoadComponent } from './viewload/viewload.component';
import { BuildLoadComponent } from './buildload/buildload.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable/release/index.js';
import {GooglePlaceModule} from '../../shared/google-places/ng2-google-place.module';
import {GooglePlaceService} from '../../shared/google-places/ng2-google-place.service';
import { Ng2DatetimePickerModule } from 'ng2-datetime-picker';
import { Config } from '../../shared/config/env.config';


//for angular restangular
import { RestangularModule } from 'ng2-restangular';

//for authentication
import { AuthGuard } from './../../shared/auth/auth-guard.service';

// Function for settting the default restangular configuration
export function RestangularConfigFactory(RestangularProvider:any) {

  RestangularProvider.setDefaultHeaders(
    { 'X-DreamFactory-Api-Key': Config.APP_KEY },{ 'X-DreamFactory-Session-Token': localStorage.getItem("id_token") }
    );


}


@NgModule({
  imports: [ BuildLoadRoutingModule, CommonModule,FormsModule,ReactiveFormsModule, LoadRoutingModule, GooglePlaceModule, NgxDatatableModule,Ng2DatetimePickerModule,RestangularModule.forRoot(RestangularConfigFactory)],
  declarations: [LoadComponent, ViewLoadComponent, BuildLoadComponent ],
  providers:[GooglePlaceService, AuthGuard ],
  exports: [LoadComponent],
})
export class LoadModule { }
