import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoadComponent } from './load.component';
import { BuildLoadComponent } from './buildload/buildload.component';
import { ViewLoadComponent } from './viewload/viewload.component';
//for authentication
import { AuthGuard } from './../../shared/auth/auth-guard.service';


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'viewload', component: ViewLoadComponent,canActivate:[AuthGuard],
    children: [
      { path: 'buildload', component: BuildLoadComponent,canActivate:[AuthGuard]},
    ]
   }
    ])
  ],
  exports: [RouterModule]
})
export class LoadRoutingModule { }
