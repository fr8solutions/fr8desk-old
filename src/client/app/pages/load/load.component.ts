import { Component, ViewEncapsulation, OnInit, style, state, animate, transition, trigger } from '@angular/core';


/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-load',
  templateUrl: 'load.component.html',
  styleUrls: ['load.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    )
  ],
})
export class LoadComponent implements OnInit {
 show: boolean = false;
  constructor(  ) {

  }

ngOnInit() {

}

 }
