import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoadComponent } from './../load.component';
import { BuildLoadComponent } from './buildload.component';
import { AuthGuard } from './../../../shared/auth/auth-guard.service';


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'buildload', component: BuildLoadComponent,canActivate:[AuthGuard],
   }
    ])
  ],
  exports: [RouterModule]
})
export class BuildLoadRoutingModule { }
