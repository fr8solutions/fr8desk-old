import { Component } from '@angular/core';
import {Restangular} from 'ng2-restangular';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import {GooglePlaceService} from '../../../shared/google-places/ng2-google-place.service';
//import { Ng2DatetimePickerModule } from 'ng2-datetime-picker.module';



@Component({
  moduleId: module.id,
  selector: 'build-load',
  templateUrl: 'buildload.html',
  styleUrls: ['buildload.css']
})
export class BuildLoadComponent {
  
   
   public options = {
                        types: ['(cities)'],
                        componentRestrictions: { country: 'IN' }
                     };
   

constructor(private restangular: Restangular,fb: FormBuilder) {


              
  }


regVerForm(bform: any) {

  this.restangular.all('user/register').post(bform.value);

}
  ngOnInit() {
  
  }


 }