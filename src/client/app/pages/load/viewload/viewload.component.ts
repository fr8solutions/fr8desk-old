import { Component, ViewChild } from '@angular/core';
import {Restangular} from 'ng2-restangular';
import { Config } from '../../../shared/config/env.config';


@Component({
  moduleId: module.id,
  selector: 'view-load',
  templateUrl: 'viewload.html',
 styleUrls: ['viewload.css']
})
export class ViewLoadComponent {

  selected: any[] = [];
  @ViewChild('myTable') table: any;
   public allServices: any;
   columns = [
    { prop: 'Load_Id' },
     { prop: 'Name' },
    { prop: 'Phone' },
    { prop: 'Source' },
    { prop: 'Destination' },
    { prop: 'Required_Date' },
    { prop: 'Material_Type' },
    { prop: 'Expected_Price' },
    { prop: 'Truck_Type' }
  
  ];

constructor(private restangular: Restangular) {
   this.restangular.withConfig((RestangularConfigurer:any) => {
      RestangularConfigurer.setDefaultHeaders(
        { 'X-DreamFactory-Api-Key': Config.APP_KEY ,'X-DreamFactory-Session-Token': localStorage.getItem("id_token") });
    }).all('ohodb/_table/load_company').getList().subscribe((services:any) => {
      this.allServices = services;});
  }

  ngOnInit() {
    // GET http://api.test.local/v1/users/2/accounts
    this.allServices = this.restangular.all('ologdb/_table/_Service_List').getList();

   

  }


 }
