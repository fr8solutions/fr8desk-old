import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoadComponent } from './../load.component';
import { ViewLoadComponent } from './viewload.component';
import { AuthGuard } from './../../../shared/auth/auth-guard.service';


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'viewload', component: ViewLoadComponent,canActivate:[AuthGuard],
   }
    ])
  ],
  exports: [RouterModule]
})
export class BuildLoadRoutingModule { }
