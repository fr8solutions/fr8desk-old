import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoadComponent } from './load/load.component';

import { BuildLoadComponent } from './load/buildload/buildload.component';
import { ViewLoadComponent } from './load/viewload/viewload.component';


//for authentication
import { AuthGuard } from './../shared/auth/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      /* define app module routes here, e.g., to lazily load a module
         (do not place feature module routes here, use an own -routing.module.ts in the feature instead)
       */
      { path: 'viewload', component: ViewLoadComponent ,canActivate:[AuthGuard],
        children: [
      { path: 'buildload', component: BuildLoadComponent,canActivate:[AuthGuard] },
    ]
   },
    ])
  ],
  exports: [RouterModule]
})
export class PagesRoutingModule { }

