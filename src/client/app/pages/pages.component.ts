import { Component } from '@angular/core';



/**
 * This class represents the main application component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-pages',
  templateUrl: 'pages.component.html',
   styleUrls: ['pages.component.css']
})
export class PagesComponent {
    
  
}
