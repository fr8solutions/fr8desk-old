import { NgModule } from '@angular/core';

import { LoadModule } from './../pages/load/load.module';

import { RestangularModule } from 'ng2-restangular';

//for authentication
import { AuthGuard } from './../shared/auth/auth-guard.service';
import { PagesComponent } from './pages.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';


@NgModule({
  imports: [ LoadModule ],
  declarations: [PagesComponent],
  providers: [AuthGuard],
  bootstrap: [PagesComponent]

})
export class PagesModule { }
