import { Component } from '@angular/core';
import { Config } from './shared/config/env.config';
import './operators';



/**
 * This class represents the main application component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-app',
  templateUrl: 'app.component.html',
   styleUrls: ['app.component.css']
})
export class AppComponent {
  
private _opened: boolean = true;

  private _toggleOpened(): void {
    this._opened = !this._opened;
  }
  //for alert

  public options = {
    position: ["top", "right"],
    timeOut: 5000,
    lastOnBottom: true
}



  
  constructor() {
    console.log('Environment config', Config);
  }
  

  

  
  
}
