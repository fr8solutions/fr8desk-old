import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MainHomeComponent } from './main_home.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: MainHomeComponent }
    ])
  ],
  exports: [RouterModule]
})
export class MainHomeRoutingModule { }
