import { Component, OnInit, trigger, transition, style, animate, state, group } from '@angular/core';
import {Renderer} from '@angular/core';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'main_home',
  templateUrl: 'main_home.component.html',
  styleUrls: ['main_home.component.css'],
  animations: [
  trigger('fadeInOut', [
    transition(':enter', [   // :enter is alias to 'void => *'
      style({opacity:0}),
      animate(500, style({opacity:1})) 
    ]),
    transition(':leave', [   // :leave is alias to '* => void'
      animate(100, style({opacity:0})) 
    ])
  ])
]
})
export class MainHomeComponent implements OnInit {
  constructor( private render: Renderer ){

  }
   show:boolean = false;
  ngOnInit() {
   // this.getNames();
  }

  userTypeSelect(event:any){
        event.preventDefault()
        this.render.setElementClass(event.target,"active",false);
      }

}
