import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainHomeComponent } from './main_home.component';
import { MainHomeRoutingModule } from './main_home-routing.module';

@NgModule({
  imports: [CommonModule, MainHomeRoutingModule],
  declarations: [MainHomeComponent],
  exports: [MainHomeComponent]
})
export class MainHomeModule { }
