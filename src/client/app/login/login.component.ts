import { Component, OnInit } from '@angular/core';
import {Restangular} from 'ng2-restangular';
import { ActivatedRoute,Router } from '@angular/router';
import 'rxjs/Rx';
import { FormGroup,FormBuilder, Validators,  FormControl } from '@angular/forms';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { AuthenticationService } from '../shared/auth/auth-service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import {NotificationsService} from 'angular2-notifications';

//import { Modal } from 'angular2-modal/plugins/bootstrap';





@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: 'login.html',
  styleUrls: ['login.css'],
})
export class Login {
   public data: Array<any> = [];
   private storeToken: any;
   private logError: any;
   private email:any;
   private password: any;
       public token: string;

  constructor( private activatedRoute: ActivatedRoute,public fb: FormBuilder, public restangular: Restangular
  , private router: Router, private auth: AuthenticationService,private _service: NotificationsService ) {}
  /*
loginForm (form:any){
    this.email = document.getElementById('email');
    this.password = document.getElementById('password');

    this.auth.login( this.email.value, this.password.value );
    
    this.router.navigate(["/login"]);
    //this.reset();
 
}
*/
loginForm (form:any){
    this.email = document.getElementById('email');
    this.password = document.getElementById('password');
    var session_token: any;
     this.auth.login(form.value).subscribe(( res:any ) => {
          this.router.navigate(["/load"],{ relativeTo: this.activatedRoute });
                //storing as JSON String
               this._service.success(
    'Login Success.., Thank you for Loggedin',
    '',
    {
        timeOut: 2000,
        showProgressBar: false,
        pauseOnHover: false,
        clickToClose: false,
        maxLength: 10
    }
)
               
                //return true;
            },(error:any)=>
            {
              //show error login
              this._service.error(
    'Login Failure.., Please Check Your Credentials or SignUp',
    '',
    {
        timeOut: 2000,
        showProgressBar: false,
        pauseOnHover: false,
        clickToClose: false,
        maxLength: 10
    }
)
            }
            );
     
     
}
/*
    var resp = this.restangular.all('user/session').post(form.value).subscribe((res: Response) => {
     let session_token = res.session_token;
     // store username and jwt token in local storage to keep user logged in between page refreshes
    //localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
     window.localStorage['session'] = session_token;
    },
     //var session_token = data.session_token;
     //localStorage.setItem("access_token",resultsToken)
     (err: any) => {
                //Here you can catch the error
            },
            () => {this.router.navigate(['home'])}
        );
    
    //alert(resp.json());
    //this.router.navigate(["/login"]);
    //this.reset();
 
}*/


}
