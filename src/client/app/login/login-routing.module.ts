import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Login } from './login.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'login', component: Login }
    ])
  ],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
