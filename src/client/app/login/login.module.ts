import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Login } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from '../shared/auth/auth-service';


@NgModule({
  imports: [CommonModule, LoginRoutingModule, RouterModule, FormsModule, ReactiveFormsModule],
  declarations: [Login],
  providers: [AuthenticationService],
  exports: [Login]
})
export class LoginModule { }
