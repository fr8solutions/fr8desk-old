import { Component, OnInit } from '@angular/core';
import { Restangular } from 'ng2-restangular';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import { AuthenticationService } from '../shared/auth/auth-service';
import { Http, Headers, Response, RequestOptions } from '@angular/http';




@Component({
    moduleId: module.id,
    selector: 'register',
    templateUrl: 'register.html',
    styleUrls: ['register.css'],
})
export class Register {
    private data1: any;
    private email: any = '';
    private show: boolean = false;
    private code: any;
    state: any = true;
    private regstep1: any = 'register';
    private regstep2: any = 'email_verify';
    constructor( private auth: AuthenticationService, private _flash: NotificationsService, public fb: FormBuilder, public restangular: Restangular, private router: Router, private activatedRoute: ActivatedRoute) {
        console.log(router.parseUrl(router.url));

        activatedRoute.queryParams
            .subscribe(params => {
                this.email = params['email'];
                this.code = params['code'];
            });

        if (this.email == undefined) {
            this.show = false;
        } else {
            this.show = true;
        }
    }


    regForm(rform: any) {
        this.auth.register(rform.value)
        .subscribe((res: any) => {
            //storing as JSON String
             //this.router.navigate(["/register"]);
             this.router.navigate(["/"]);
            this._flash.success(
                'Registration success please check your Email to Activate Account',
                '',
                {
                    timeOut: 2000,
                    showProgressBar: false,
                    pauseOnHover: false,
                    clickToClose: false,
                    maxLength: 10
                }
            )
           
            //return true;
        }, (error: any) => {
                //show error login
            }
        );
    }

    regVerForm(verform: any) {

        this.auth.verify(verform.value)
        .subscribe((res: any) => {
            //storing as JSON String
            this.router.navigate(["/load"],{ relativeTo: this.activatedRoute });
            this._flash.success(
                'Account Activated.., Thank You...!',
                '',
                {
                    timeOut: 2000,
                    showProgressBar: false,
                    pauseOnHover: false,
                    clickToClose: false,
                    maxLength: 10
                }
            )
            
            //return true;
        }, (error: any) => {
                //show error login
            }
        );
    }
}
