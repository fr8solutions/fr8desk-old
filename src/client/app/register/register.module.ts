import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Register } from './register.component';
import { RegisterRoutingModule } from './register-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';





@NgModule({
  imports: [CommonModule, RegisterRoutingModule, RouterModule, FormsModule, ReactiveFormsModule],
  declarations: [Register],
  exports: [Register]
})
export class RegisterModule { }
