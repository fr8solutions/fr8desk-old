import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Register } from './register.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'register', component: Register }
    ])
  ],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
